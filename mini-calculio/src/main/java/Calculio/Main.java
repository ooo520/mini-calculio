package Calculio;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Supplier;


public class Main {
    static int nombre_boucles = 10; // devrait toujours être égale à tricky.length
    // static HashMap<String, Boolean> lists = new HashMap<>(); // stock les calculs pour éviter des  répétitions
    static Map<String ,BigDecimal> tricky = new HashMap<>();
    static  String question_actuelle = null;
    static int nombre_propositions = 4;
    private static BigDecimal generate_calcul_A() {
        Character[] ops = new Character[]{'+','-','*','/'};
        int val =(int) (Math.floor(Math.random()*ops.length )); // nombre entre 0 et 3
        int number1 = (int) (Math.floor(Math.random() * 9.99) +1); // repartition pseudo-égales entre 1 et 10
        int number2 = (int) (Math.floor(Math.random() * 9.99) +1);
        if (val == 3 && number2 == 0 )
            number2 = 1;
        question_actuelle = number1+" " + ops[val]+" " + number2 +"?";
        return answer(new BigDecimal(number1), ops[val],new BigDecimal(number2),true);
    }
    private static BigDecimal generate_calcul_B() {
        Character[] ops = new Character[]{'+','-','*','/'};
        int val =(int) (Math.floor(Math.random()*ops.length )); // nombre entre 0 et 3
        int number1 = (int) (Math.floor(Math.random() * 100 - 50));
        int number2 = (int) (Math.floor(Math.random() * 100 - 50));
        if (val == 3 && number2 == 0 )
            number2 = 1;
        question_actuelle = number1 + " " + ops[val] +" "+ number2+"?";
        return answer(new BigDecimal(number1), ops[val],new BigDecimal(number2),true);
    }
    private static BigDecimal generate_calcul_C() {
        Character[] ops = new Character[]{'+','-','*','/','^','!'};
        float number2 = 0,number1;
        int val =(int) (Math.floor(Math.random()*ops.length )); // nombre entre 0 et 5
        number1 = (float)(Math.floor((Math.random() * 100 - 50)* 100 ) / 100); // arrondi au centième
        if (val == 5) // pas besoin de number2 et number1 doit être un entier positif
        {
            number1 = (int) (Math.floor(Math.random() * 10));
            question_actuelle = String.valueOf(number1) + ops[val]+'?';

	    return answer(new BigDecimal(number1), ops[val],new BigDecimal(number2),true);
        }
        else if (val == 4) // puissance donc number2 doit être un entier positif
        {
            number1 = (int) (Math.floor(Math.random() * 100 - 50));
            number2 = (int) (Math.floor(Math.random() * 4));
        }
        else
            number2 = (float) (Math.floor((Math.random() * 100 - 50) * 100) / 100);
        if (val == 3 && number2 == 0 )
            number2 = 1;
        question_actuelle =String.valueOf(number1)+' '+ ops[val] +' '+String.valueOf(number2) + '?';
        // System.out.println("generation :"+ number1 +" "+number2;
        return answer(new BigDecimal(String.valueOf(number1)), ops[val],new BigDecimal(number2),false);
    }
    private static BigDecimal generate_calcul_D() {
        if (tricky.isEmpty()) {
            tricky.put("Soit X une variable aléatoire, avec f(x) = 2(1-x) si x appartient à [0;1]," +
                    " que vaut l’espérance de X",new BigDecimal("0.33"));
            tricky.put("Soit X une variable aléatoire, avec f(x) = exponentielle de -x si x supérieur ou égal à 0," +
                    " que vaut l’espérance de X",new BigDecimal(1));
            tricky.put("Nathan a 10 ans. Il achète 4 CD à 19 euros pièces. Il paye avec 2 billets de 50 euros."+
                    "Combien lui restera-t-il d'euros?", new BigDecimal(24));
            tricky.put("Combien vaut 5*cos(0)? résultat en radiant",new BigDecimal(5));
            tricky.put("Combien vaut 1 à la puissance 0",new BigDecimal(1));
            tricky.put("Combien vaut 0 à la puissance 0",new BigDecimal(1));
            tricky.put("Soit f(x), la suite de fibonacci, f(x) = f(x-1)+f(x-2) avec f(0) = 0 et f(1)=1\n"+
                    "Combien vaut f(8)",new BigDecimal(21));
            tricky.put("Combien vaut la racine cube de 8, soit x à la puissance 3 = 8",new BigDecimal(2));
            tricky.put("Soit f(x), la suite de fibonacci, f(x) = f(x-1)+f(x-2) avec f(0) = 0 et f(1)=1\n"+
                    "Combien vaut f(5)",new BigDecimal(5));

            tricky.put("Quelle est la réponse à la grande question sur la vie, l’univers et le reste ? ",new BigDecimal(42));
        }
        if (tricky.size() < nombre_boucles)
            System.err.println("Fort risque d’accident dû à un manque de questions");
        boolean b = question_actuelle == null;
        for (String k: tricky.keySet()) {
            if (b)
            {
                question_actuelle = k;
                return tricky.get(k);
            }
            if (question_actuelle.equals(k))
                b = true;
        }
        // si on arrive ici, ce n’est qu’ils n’y à pas assez de questions
        return null;
    }
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Supplier<BigDecimal> generateur_calcul = null;
        Function<BigDecimal, Boolean> generateur_reponses = null;
        // main.java.Calculio.Calculio c = new main.java.Calculio.Calculio(300,300);

        boolean b;
        Scanner in = new Scanner(System.in);
        String s;
        do {

            do{
                b = false;
                System.out.flush();
                System.out.print("""
                        Choisissez une difficulté !
                        A: Basic
                        B: Modeste :)
                        C: EDD
                        D: La réponse D
                        """
                );
                System.out.println("Veuillez entrer A,B,C ou D seulement.");

                s = in.next();
                switch (s.toUpperCase()) {
                    case "A" -> generateur_calcul = Main::generate_calcul_A;
                    case "B" -> generateur_calcul = Main::generate_calcul_B;
                    case "C" -> generateur_calcul = Main::generate_calcul_C;
                    case "D" -> generateur_calcul = Main::generate_calcul_D;
                    default -> b = true;
                }
                question_actuelle = null; // met par default pour éviter des bugs de boucles
            } while (b);
            do {
                b = false;
                // le ' !' est proposé par le dico français IntelliJ
                System.out.print("""
                        Choisissez un mode d’input !
                        1 : Manuelle
                        2 : Propositions °o°
                        """
                );
                System.out.println("Veuillez entrer 1 ou 2 seulement.");
                s = in.next();
                switch (s) {
                    case "1" -> generateur_reponses = (val) -> get_input(in, val);
                    case "2" -> generateur_reponses = (val) -> make_input(in, val);
                    default -> b = true;
                }
            } while (b);
            loop(nombre_boucles, generateur_calcul, generateur_reponses);
            System.out.println("Merci d’avoir joué");
            System.out.println("Voulez-vous rejouer? entrer non pour fermer la partie");
            s = in.next();
        }while (!s.equals("non"));
    }


    private static BigDecimal ajustement(boolean entier)
    { // valeur au hasard entre 0 et 10 avec 2 chiffre apres la virgule
        if (entier)
        {
            return BigDecimal.valueOf(Math.floor(Math.random() * 10.99) + 1).round(new MathContext(0,RoundingMode.DOWN));
        }
        return BigDecimal.valueOf(Math.floor(Math.random() * 10.99 * 100) / 100 + 1).round(new MathContext(2,RoundingMode.DOWN));
    }
    private static boolean make_input(Scanner in, BigDecimal val)
    {
        int pos = (int) Math.floor(Math.random() * 3.50); // valeur entre 0 et 3 avec relativement autant de chance
        BigDecimal[] proposition = new BigDecimal[4];
        proposition[pos] = val;
        boolean entier = new BigDecimal(val.intValue()).compareTo(val) == 0; // vérifie si la valeur de retour est un entier
        for (int i = pos - 1; i >= 0 ; i--)
        {
                proposition[i] = proposition[i+1].subtract(ajustement(entier));
                if (entier)
                    proposition[i]  = formatage(true,proposition[i]); // retire les zéros après la virgule uniquement
        }
        for (int i = pos + 1; i < nombre_propositions; i++ )
        {
            proposition[i] = proposition[i-1].add(ajustement(entier));
            if (entier)
                proposition[i]  = formatage(true,proposition[i]); // retire les zéros après la virgule uniquement
        }
        char a = 'A';
        System.out.println("Veuillez choisir en entrant A, B, C ou D uniquement");
        for (int i = 0; i < nombre_propositions; i++)
        {
            System.out.println((char) (a+i)+ ":" + proposition[i]);
        }
        String s = in.next().toUpperCase();
        while (s.charAt(0) > 'D' || s.charAt(0) < 'A' ) // le regex : '^[ABCD]' ne marchait pas
        {
            System.out.print("input :"+ s.charAt(0) +"\n");
            System.out.println("La réponse n’est pas A, B, C ou D");
            s = in.next();
        }
        return s.toCharArray()[0] == a+pos;
    }
    private static boolean get_input(Scanner in, BigDecimal val)
    {
        System.out.println(
                            """
                            Faites attention à ne pas laisser des zéros en trop
                            ou utilisé, au lieu de .
                            Pour les difficultés C et D, il y a des valeurs décimales, elles sont arrondies aux centièmes à la valeur inférieure donc pour 0.1234 écrire 0.123.
                            """
        );
        String rep = in.next();
        return val.toPlainString().equals(rep);
    }
    private static void loop(int l, Supplier<BigDecimal> calculateur, Function<BigDecimal, Boolean> reponses) {
        int score = 0;
        BigDecimal reponse;
        for (int i =0; i < l ; i++)
        {
            reponse = calculateur.get();
            if (question_actuelle == null)
                throw new RuntimeException("Erreur lors de la génération de questions");
            else
            {
                System.out.println(question_actuelle);
                if (reponses.apply(reponse))
                {
                    System.out.println("Correct");
                    score++;
                }
                else {
                    System.out.println("La réponse était " + reponse.toPlainString());
                }
                System.out.println("score :"+score);

            }
        }
        System.out.println("Résultat : "+score +" / 10.");

    }
    private  static BigDecimal formatage(boolean entier, BigDecimal valeur)
    {
        return entier?
                new BigDecimal( valeur.stripTrailingZeros().toPlainString()) :
                new BigDecimal(valeur.setScale(2,RoundingMode.DOWN).stripTrailingZeros().toPlainString());
    }

    private static BigDecimal answer(BigDecimal l, Character op, BigDecimal r, boolean entier)
    {
        // System.out.println("try to answer :"+ l.toPlainString() +' ' +op + ' '+r.toPlainString() );

        switch (op) {
            case '*' -> {
                return formatage(entier,l.multiply(r));
            }
            case '/' -> {
                assert(!r.equals(BigDecimal.ZERO));
                return formatage(entier,l.divide(r,2,RoundingMode.DOWN));
            }
            case '-' -> {
                return formatage(entier,l.subtract(r));
            }
            case '+' -> {
                return formatage(entier,l.add(r));
            }
            case '^' -> {
                if (r.compareTo(new BigDecimal(Integer.MAX_VALUE)) > 0)
                    return null;
                return formatage(entier,l.pow(r.toBigInteger().intValue()));
            }
            case '!' -> // r est ignoré
            {
                int val = l.toBigInteger().intValue();
                int acc = 1;
                for (int i = 2; i <= val; i++)
                    acc*= i;
                return formatage(entier,new BigDecimal(acc));
            }
        }
        return null;
    }
}
